class ReminderErr(Exception):

    def __init__(self, wrong_attribute, message):
        self.wrong_attribute = wrong_attribute
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        if self.wrong_attribute is None:
            return self.message
        return f"{self.message} ({self.wrong_attribute})"


class ReminderTagErr(ReminderErr):
    message = "Format of a tag is wrong!"

    def __init__(self, wrong_attribute):
        super().__init__(wrong_attribute, self.message)


class ReminderDateTimeErr(ReminderErr):
    message = "Format of the Date&Time parameter is wrong!"

    def __init__(self, wrong_attribute):
        super().__init__(wrong_attribute, self.message)


class ReminderExistsErr(ReminderErr):
    message = "The same reminder already exists!"

    def __init__(self):
        super().__init__(None, self.message)


class ReminderDoesntExistErr(ReminderErr):
    message = "The reminder doesn't exist!"

    def __init__(self):
        super().__init__(None, self.message)


# TODO this is a mess, mby let the caller pass the message