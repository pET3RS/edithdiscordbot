import os
from discord import Embed
import asyncio
from discord.ext import commands, tasks
from dotenv import load_dotenv
from exceptions import *
from tinydb import TinyDB, where
import re
from datetime import date, datetime, timedelta
from math import ceil
import traceback


class Reminder:
    """
    Represent a reminder for use with discord bot.
    """

    def __init__(self, author, date_time, contents):

        self._id = None

        self._check_tag(author)
        self._author = author
        self._tags = set()

        self._date_time = None
        self._date_time = self._check_date_time(date_time)

        self._text = contents

        self.reminder_channel = None

    def change_id(self, new_id):
        """
        Set the id of the reminder
        :param new_id: integer representing the id
        """
        self._id = new_id

    def get_id(self):
        """
        Get the current id of the reminder
        :return: integer representing the id or None
        """
        return self._id

    def get_author(self):
        """
        Get the author of the reminder (Can't be changed).
        Mind that the tag can have multiple forms -- see the _check_tag() method.
        :return: string with authors discord tag
        """
        return self._author

    def add_tag(self, tag):
        """
        Adds the specified tag to the reminder which is used to tag when reminding. The tag
        is validated before assigning.
        Mind that the tag can have multiple forms -- see the _check_tag() method.
        :param tag: string containing a discord tag
        """
        self._check_tag(tag)
        self._tags.add(tag)

    def remove_tag(self, tag):
        """
        Removes the specified tag from the set of tags to use when reminding.
        The tag is checked before removal.
        Mind that the tag can have multiple forms -- see the _check_tag() method.
        :param tag: string representing a discord tag
        :return:
        """
        self._check_tag(tag)
        self._tags.remove(tag)

    def remove_tags(self):
        """
        Removes all tags from the set of tags to use when reminding.
        """
        self._tags.clear()

    def get_tags(self):
        """
        Get the set of tags for reminding.
        :return: set of tags
        """
        return self._tags

    def change_date_time(self, new_date_time):
        """
        Validates the new_date_time and changes it.
        :param new_date_time: string representation of date and time - see _check_date_time() method for formats
        """
        new_date_time_checked = self._check_date_time(new_date_time)
        self._date_time = new_date_time_checked

    def get_date_time(self):
        """
        Get the date and time in string format "Ddd.mm.yyyyThh:mm"
        :return: string with date and time
        """
        return self._date_time

    def change_text(self, new_text):
        """
        Changes the description of the reminder.
        :param new_text: string containing the new description for the reminder
        """
        self._text = new_text

    def get_text(self):
        """
        Get the reminder description.
        :return: string containing the reminder description
        """
        return self._text

    def get_db_dict(self):
        """
        Creates the dictionary used to store the reminder in the database.
        :return: dictionary containing crucial reminder data
        """
        return {"author": self._author,
                "date_time": self._date_time,
                "tags": list(self._tags),  # The set can't be stored by the tinyDB
                "text": self._text,
                "channel": self.reminder_channel}

    def get_discord_embed(self):
        """
        Creates the discord message embed representation of the reminder
        :return:
        """
        reminder_embed = Embed(title="Reminder")
        reminder_embed.add_field(name="Author", value=self._author, inline=False)
        reminder_embed.add_field(name="Date and time", value=" ".join(self._date_time.split('T'))[1:], inline=True)
        reminder_embed.add_field(name="Tags", value="\n".join(self._tags), inline=True)
        reminder_embed.add_field(name="Description", value=self._text, inline=False)
        return reminder_embed

    def is_today(self):
        """
        Determine if the time of the reminder is today.
        :return: True if the date and time of the reminder is today and False if not.
        """
        return self._get_datetime() < datetime.now().replace(hour=23, minute=59, second=59)

    def is_in_ten_minutes(self):
        """
        Determine if the time of the reminder is within next ten minutes
        :return: True if the date and time of the reminder is within next 10 minutes and False if not.
        """
        return self._get_datetime() <= datetime.now() + timedelta(minutes=10)

    @staticmethod
    def _check_tag(tag):
        """
        Validates the discord tag format and raises an ReminderTagErr if it is invalid.
        :param tag: string representing the discord tag
        """
        tag_format_basic = re.compile(r"^.{3,32}#[0-9]{4}$")
        tag_format_with_id = re.compile(r"^<@[!&]?\d*>$")
        mass_tags = ["@everyone", "@here"]
        if tag_format_basic.match(tag) is None and tag_format_with_id.match(tag) is None and tag not in mass_tags:
            raise ReminderTagErr(tag)

    def _check_date_time(self, new_date_time):
        """
        Validate the date and time of a reminder. Raises the ReminderDateTimeErr if it is invalid.
        The date and time format can be inputted in multiple formats:
            - complete: "Ddd.mm.yyyyThh:mm" (can be used anywhere)
            - partial: (can be only used in edits of already stored reminders)
                    - date only: "Ddd.mm.yyyy"
                    - time only: "Thh:mm"
        :param new_date_time: string representation of the date and time in multiple formats
        """
        date_time_format = re.compile(r"^D\d?\d\.\d?\d\.\d\d\d\dT\d?\d:\d\d$")
        date_time_format_time_only = re.compile(r"^T\d?\d:\d\d$")
        date_time_format_date_only = re.compile(r"^D\d?\d\.\d?\d\.\d\d\d\d$")

        date_format = "%d.%m.%Y"
        time_format = "%H:%M"

        if date_time_format.match(new_date_time) is not None:

            new_date_time_list = new_date_time.split("T")
            new_date_time_list[0] = new_date_time_list[0][1:]

            try:  # Check the date and the time validity
                the_date = datetime.strptime(new_date_time_list[0], date_format)
                the_time = datetime.strptime(new_date_time_list[1], time_format)
            except ValueError:
                raise ReminderDateTimeErr(new_date_time)

            if the_date.date() < datetime.today().date() or (
                    the_date.date() == datetime.today().date() and the_time.time() <= datetime.now().time()):
                raise ReminderDateTimeErr(new_date_time)

            return new_date_time

        elif date_time_format_time_only.match(new_date_time) is not None:

            # Check the time validity
            try:
                the_time = datetime.strptime(new_date_time[1:], time_format)
            except ValueError:
                raise ReminderDateTimeErr(new_date_time)

            if the_time.time() <= datetime.now().time():
                raise ReminderDateTimeErr(new_date_time)

            # Create the complete date_time from the partial input
            if self._date_time is None:
                return f"D{date.today().strftime(date_format)}{new_date_time}"
            else:
                return f"{self._date_time.split('T')[0]}{new_date_time}"

        elif date_time_format_date_only.match(new_date_time) is not None:

            if self._date_time is None:  # Don't allow only date as initial input
                raise ReminderDateTimeErr(new_date_time)

            # Check the date validity
            try:
                the_date = datetime.strptime(new_date_time[1:], date_format)
            except ValueError:
                raise ReminderDateTimeErr(new_date_time)

            if the_date.date() < datetime.today().date():
                raise ReminderDateTimeErr(new_date_time)

            return f"{new_date_time}T{self._date_time.split('T')[1]}"

        else:
            raise ReminderDateTimeErr(new_date_time)

    def _get_datetime(self) -> datetime:
        """
        Get the date and time of the reminder as datetime object.
        :return: datetime representation of due time of the reminder
        """
        return datetime.strptime(self._date_time[1:].replace("T", " ", 1), "%d.%m.%Y %H:%M")

    def __eq__(self, reminder):
        return self._date_time == reminder.get_date_time() and self._text == reminder.get_text()

    def __ne__(self, reminder):
        return not self == reminder

    def __lt__(self, reminder):
        return self._get_datetime() < reminder._get_datetime()

    def __le__(self, reminder):
        return self._get_datetime() <= reminder._get_datetime()

    def __gt__(self, reminder):
        return self._get_datetime() > reminder._get_datetime()

    def __ge__(self, reminder):
        return self._get_datetime() >= reminder._get_datetime()

    def __str__(self):
        return f"Reminder -> id: {self._id},\n author: {self._author},\n date_time: {self._date_time},\n tags: {self._tags},\n text: {self._text}"


class ReminderDB:
    """
    Wrapper around tinydb library used as a Reminder database.
    """
    def __init__(self, file: str):
        self.tiny_db = TinyDB(file)
        self.reminders_table = self.tiny_db.table("Reminders")
        # TODO events table?

    def push(self, reminder: Reminder):
        """
        Adds a reminder to the database. If the reminder already exists raises ReminderExistsErr.
        :param reminder:
        """
        if self._reminder_exists(reminder):
            raise ReminderExistsErr()
        new_id = self.reminders_table.insert(reminder.get_db_dict())
        reminder.change_id(new_id)

    def update(self, reminder: Reminder):
        """
        Updates the reminder in the database. If the reminder doesn't exist in the db raises ReminderDoesntExistErr.
        :param reminder:
        :return:
        """
        if reminder.get_id() is None and not self._reminder_exists(reminder):
            raise ReminderDoesntExistErr()
        self.reminders_table.update(reminder.get_db_dict(), doc_ids=[reminder.get_id()])

    def delete(self, reminder: Reminder):
        """
        Deletes the specified reminder form the database. If the reminder doesn't exists raises the ReminderDoesntExistErr
        :param reminder:
        """
        if reminder.get_id() is None and not self._reminder_exists(reminder):
            raise ReminderDoesntExistErr()

        self.reminders_table.remove(doc_ids=[reminder.get_id()])
        reminder.change_id(None)  # The reminder no longer exists -> removes the id

    def id_query(self, searched_id: int):
        """
        Searches for the reminder in the database by the reminder id. If it doesn't exist raises ReminderDoesntExistErr
        :param searched_id:
        :return:
        """
        reminder_dict = self.reminders_table.get(doc_id=searched_id)

        if reminder_dict is None:
            raise ReminderDoesntExistErr()

        return self._create_reminder_from_tinydb_doc(reminder_dict)

    def date_time_query(self, date_time: str):
        """
        Searches for a reminder by date and time (for date_time formats see _check_date_time() method of Reminder class)
        :param date_time: string with date and time of the searched reminder in supported formats
        :return: list of matched Reminders or None
        """
        reminder_dict_list = self.reminders_table.search((where("date_time") == date_time))
        if reminder_dict_list == list():
            return None

        reminders = []
        for reminder_dict in reminder_dict_list:
            new_reminder = self._create_reminder_from_tinydb_doc(reminder_dict)
            if new_reminder is not None:
                reminders.append(new_reminder)
        return reminders

    def date_time_text_query(self, date_time: str, text: str):
        """
        Searches for a reminder by its date, time and text.
        (For date_time formats see _check_date_time() method of Reminder class)
        :param date_time: The date and time of the searched reminder
        :param text: The description of the reminder
        :return: None or list of reminders matching the search
        """
        reminder_dict_list = self.reminders_table.search((where("date_time") == date_time) &
                                                         (where("text") == text))
        if reminder_dict_list == list():
            return None

        reminders = []
        for reminder_dict in reminder_dict_list:
            new_reminder = self._create_reminder_from_tinydb_doc(reminder_dict)
            if new_reminder is not None:
                reminders.append(new_reminder)
        return reminders

    def get_all_upcoming_reminders(self):
        """
        Get all upcoming reminders from the database.
        :return: list of upcoming reminders
        """
        reminder_dict_list = self.reminders_table.all()

        reminders = []
        for reminder_dict in reminder_dict_list:
            new_reminder = self._create_reminder_from_tinydb_doc(reminder_dict)
            if new_reminder is not None:
                reminders.append(new_reminder)
        return reminders

    def _reminder_exists(self, reminder: Reminder):
        """
        Checks if the reminder already exists in the database
        :param reminder: reminder one is searching for
        :return: True if it exists or False if it doesn't
        """
        found = False
        if reminder.get_id() is None:
            search = self.reminders_table.search((where("date_time") == reminder.get_date_time()) &
                                                 (where("text") == reminder.get_text()))
            if search != list():
                found = True
        else:
            if self.reminders_table.get(doc_id=reminder.get_id()) is not None:
                found = True

        return found

    @staticmethod
    def _create_reminder_from_tinydb_doc(reminder_dict):
        """
        Create reminder from the dictionary returned by the database
        :param reminder_dict: dictionary representation of a reminder from the database
        :return: Reminder created from the specified dictionary or None
        """

        if reminder_dict is None:
            return None

        try:
            reminder = Reminder(reminder_dict["author"], reminder_dict["date_time"], reminder_dict["text"])
            reminder.change_id(reminder_dict.doc_id)
            for tag in reminder_dict["tags"]:
                reminder.add_tag(tag)
            reminder.reminder_channel = reminder_dict["channel"]
        except ReminderErr as err:
            print(traceback.format_exc())
            return None

        return reminder


class ReminderQueue:
    """
    Queue of reminders sorted by time and date.
    """
    # TODO add some logging

    def __init__(self):
        self._queue = list()

    def add_reminder(self, reminder: Reminder):
        """
        Adds the reminder to the queue if it already isn't in it and sorts the queue.
        If the reminder already exists in the queue raises ReminderExistsErr.
        :param reminder: The reminder one wants to add to the queue
        """
        if reminder in self._queue:
            raise ReminderExistsErr()

        self._queue.append(reminder)
        self._queue = sorted(self._queue, reverse=True)

    def remove_reminder(self, reminder):
        """
        Removes the reminder form the queue. If the reminder doesn't exist in the queue raises Reminder DoesntExistErr.
        :param reminder:
        :return:
        """
        if reminder not in self._queue:
            raise ReminderDoesntExistErr()

        self._queue.remove(reminder)

    def update_reminder_by_id(self, reminder):
        """
        Updates the reminder in the queue by matching the ID. If the reminder with same id wasn't found
        raises ReminderDoesntExistErr.
        :param reminder:
        """
        for reminder_old in self._queue:
            if reminder_old.get_id() == reminder.get_id():
                self.remove_reminder(reminder_old)
                self.add_reminder(reminder)
                return

        # The reminder with specified id doesn't exist in the queue
        raise ReminderDoesntExistErr()

    def get_next_reminder(self):
        """
        Get the next reminder from the queue. Returns None if the queue is empty or the next reminder by its date and time.
        Doesn't remove the next reminder from the queue.
        :return: None or next reminder
        """
        if self._queue == list():
            return None
        else:
            return self._queue[-1]

    def get_queue(self):
        """
        Returns the queue as a list sorted by date and time of the reminders so that the nearest
        date and time is at the end of list.
        :return: list representing the queue
        """
        return self._queue


class EdithBot(commands.Bot):

    def __init__(self, command_prefix):
        super(EdithBot, self).__init__(command_prefix=command_prefix)
        self.db = ReminderDB(DB_FILE)
        self.queue = ReminderQueue()
        self._add_commands()

        load_dotenv()
        self.token = os.getenv('TOKEN')

    def _add_commands(self):
        @self.group(pass_context=True, invoke_without_command=True)
        async def reminder(ctx):
            embed = Embed(title="Reminder command",
                          description="A way of setting and managing your reminders through a discord bot.",
                          color=0xFF5733)
            embed.add_field(name="create [date&time] [@TAGs] [text]",
                            value="Creates a new reminder."
                                  "\n\t``[datetime]`` - a date and time of the reminder in format ``Ddd.mm.yyyyThh:mm``"
                                  "\n\t``[@TAGs]``    - a list of tags"
                                  "\n\t``[text]``     - a reminder description in double-quotes",
                            inline=False)
            embed.add_field(name="remove [id]",
                            value="Removes the selected reminder"
                                  "\n\t``[id]`` - an id, which can be found in ``!reminder list``",
                            inline=True)
            embed.add_field(name="list",
                            value="Lists the all upcoming reminders.",
                            # "\n\t[period] - optional (*) argument, which limits the output of this command "
                            # "by narrowing the period of upcoming reminders to ``day/week/month/year``",
                            inline=False)
            embed.add_field(name="edit [id] [datetime] [@TAGs] [text]",
                            value="Edits the whole reminder by overwriting its parts.",
                            inline=False)
            embed.add_field(name="edit datetime [id] [datetime]",
                            value="Edits the date and the time of the reminder."
                                  "\n\t[datetime] - specifies the new date and time ``Ddd.mm.yyyyThh:mm``,"
                                  "this also supports partial change of the datetime parameter by leaving "
                                  "the other part at the original value (Using ``Ddd.mm.yyyy`` or ``Thh:mm`` only).",
                            inline=False)
            embed.add_field(name="edit tags [id] [@TAGs]",
                            value="Overwrites the tags of the reminder",
                            inline=False)
            embed.add_field(name="edit text [id] [text]",
                            value="Overwrites the description of the reminder.",
                            inline=False)
            await ctx.channel.send(embed=embed)

        @reminder.command(pass_context=True)
        async def create(ctx, *args):

            if not args or len(args) < 3:
                await ctx.channel.send("Missing arguments!")
                return

            date_time = args[0]
            desc = args[-1]

            try:
                # create the reminder
                new_reminder = Reminder(f"<@!{ctx.message.author.id}>", date_time, desc)
                for tag in args[1:len(args) - 1]:
                    new_reminder.add_tag(tag)

                new_reminder.reminder_channel = ctx.channel.id

                # add to the database
                self.db.push(new_reminder)
            except ReminderErr as err:
                await ctx.channel.send(f"Reminder creation error: {err}")
                # print(traceback.format_exc())
                return

            # add to current queue
            #if new_reminder.is_in_ten_minutes():
            # everything goes to the q and the executor selects what he waits for
            self.queue.add_reminder(new_reminder)

            await ctx.channel.send(embed=new_reminder.get_discord_embed())

        @reminder.command(pass_context=True)
        async def info(ctx, *args):

            if not args or not args[0].isnumeric():
                await ctx.channel.send("Missing reminder ID argument!")
                return

            reminder_id = int(args[0])

            try:
                reminder = self.db.id_query(reminder_id)
            except ReminderDoesntExistErr:
                await ctx.channel.send(f"Reminder with ID:{reminder_id} doesn't exist!")
                return

            await ctx.channel.send(embed=reminder.get_discord_embed())

        @reminder.command(pass_context=True)
        async def remove(ctx, *args):

            if not args or not args[0].isnumeric():
                await ctx.channel.send("Missing reminder ID argument!")
                return

            reminder_id = int(args[0])

            try:
                # find and remove from DB
                reminder = self.db.id_query(reminder_id)
                self.db.delete(reminder)

                # find and remove from queue if exists there
                if reminder in self.queue.get_queue():
                    self.queue.remove_reminder(reminder)
            except ReminderDoesntExistErr:
                await ctx.channel.send(f"Reminder with ID:{reminder_id} doesn't exist!")
                return

            await ctx.channel.send("Removed the reminder!")

        # EDIT
        @reminder.group(pass_context=True, invoke_without_command=True)
        async def edit(ctx, *args):

            if not args or not args[0].isnumeric() or len(args) < 4:
                await ctx.channel.send("Wrong arguments!")
                return

            reminder_id = int(args[0])

            date_time = args[1]
            desc = args[-1]

            try:
                # create the reminder
                new_reminder = Reminder(f"<@!{ctx.message.author.id}>", date_time, desc)
                for tag in args[1:len(args) - 1]:
                    new_reminder.add_tag(tag)
                new_reminder.change_id(int(reminder_id))

                # update in the database
                self.db.update(new_reminder)

                # update in the queue if it exists
                try:
                    self.queue.update_reminder_by_id(new_reminder)
                except ReminderDoesntExistErr:
                    # add to current queue if needed
                    if new_reminder.is_today():
                        self.queue.add_reminder(new_reminder)

            except ReminderErr as err:
                await ctx.channel.send(f"Reminder error: {err}")
                return

            await ctx.channel.send("The reminder was successfully edited!")

        @edit.command(pass_context=True)
        async def datetime(ctx, *args):

            if not args or not args[0].isnumeric() or len(args) < 2:
                ctx.channel.send("Missing or wrong arguments!")
                return

            reminder_id = int(args[0])
            date_time = args[1]

            try:
                # find the reminder
                reminder = self.db.id_query(reminder_id)

                # update the database
                reminder.change_date_time(date_time)
                self.db.update(reminder)

                # update in the queue if it exists
                try:
                    self.queue.update_reminder_by_id(reminder)
                except ReminderDoesntExistErr:
                    # add to current queue if needed
                    # TODO this could surpass the queues time range, which could potentially cause double remind
                    #  could be fixed by remembering the queue time window and adding this reminder in based on that
                    if reminder.is_in_ten_minutes():
                        self.queue.add_reminder(reminder)

            except ReminderErr as err:
                await ctx.channel.send(f"Reminder error: {err}")
                print(traceback.format_exc())
                return

            await ctx.channel.send("The reminder was successfully edited!")

        @edit.command(pass_context=True)
        async def tags(ctx, *args):

            if not args or not args[0].isnumeric() or args < 3:
                await ctx.channel.send("Wrong or missing arguments!")
                return

            reminder_id = int(args[0])
            tags = args[1:]

            try:
                # find the reminder
                reminder = self.db.id_query(reminder_id)

                # update the reminder
                reminder.remove_tags()
                for tag in tags:
                    reminder.add_tag(tag)

                # update in the database
                self.db.update(reminder)

                # update in the queue if it exists
                try:
                    self.queue.update_reminder_by_id(reminder)
                except ReminderDoesntExistErr:
                    pass

            except ReminderErr as err:
                await ctx.channel.send(f"Reminder error: {err}")
                print(traceback.format_exc())
                return

            await ctx.channel.send("The reminder was successfully edited!")

        @edit.command(pass_context=True)
        async def text(ctx, *args):

            if not args or len(args) < 2 or not args[0].isnumeric():
                ctx.channel.send("Missing or wrong arguments!")
                return

            reminder_id = int(args[0])
            text = args[1]

            try:
                # find the reminder
                reminder = self.db.id_query(reminder_id)

                # update the database
                reminder.change_text(text)
                self.db.update(reminder)

                # update in the queue if it exists
                try:
                    self.queue.update_reminder_by_id(reminder)
                except ReminderDoesntExistErr:
                    # doesn't need to be added since the date and time didn't change
                    pass

            except ReminderErr as err:
                await ctx.channel.send(f"Reminder error: {err}")
                print(traceback.format_exc())
                return

            await ctx.channel.send("The reminder was successfully edited!")

        # LIST
        @reminder.command(pass_context=True)
        async def list(ctx):

            buttons = [u"\u23EA", u"\u2B05", u"\u27A1", u"\u23E9"]  # skip to start, left, right, skip to end
            current_page = 0
            page_size = 7

            embed_page = Embed(title="List of upcoming reminders")
            upcoming_reminders = self.db.get_all_upcoming_reminders()
            if not upcoming_reminders:
                await ctx.channel.send("There are no upcoming reminders!")
                return

            # create embed pages from the upcoming reminders
            embed_pages = []
            for i in range(1, len(upcoming_reminders) + 1):

                embed_page.add_field(
                    name=f"ID: {upcoming_reminders[i - 1].get_id()}\t\t\t\tDate&Time: {' '.join(upcoming_reminders[i - 1].get_date_time().split('T'))[1:]}",
                    value=upcoming_reminders[i - 1].get_text(), inline=False)

                if i % page_size == 0:
                    embed_pages.append(embed_page)
                    embed_page = Embed(title="List of upcoming reminders")

            # append the last page
            num_of_pages = ceil(len(upcoming_reminders) / page_size)
            if num_of_pages != len(embed_pages):
                embed_pages.append(embed_page)

            for i, page in enumerate(embed_pages):
                page.set_footer(text=f"Page: {i + 1}/{num_of_pages}")

            # send the first page and add the buttons for navigation
            list_message = await ctx.channel.send(embed=embed_pages[current_page])

            # if there is only one page, the button functionality isn't needed
            if num_of_pages <= 1:
                return

            # Buttons for page swapping
            for button in buttons:
                await list_message.add_reaction(button)

            # loop, which gives the reactions button functionality
            while True:
                try:
                    reaction, user = await bot.wait_for("reaction_add", check=lambda reaction,
                                                                                     user: user == ctx.author and reaction.emoji in buttons,
                                                        timeout=60.0)
                except asyncio.TimeoutError:
                    current_page_embed = embed_pages[current_page]
                    current_page_embed.set_footer(text="Buttons timed out.")
                    await list_message.edit(embed=current_page_embed)
                    await list_message.clear_reactions()
                else:
                    previous_page = current_page

                    # decide which page is next
                    if reaction.emoji == u"\u23EA":
                        current_page = 0
                    elif reaction.emoji == u"\u2B05" and current_page > 0:
                        current_page -= 1
                    elif reaction.emoji == u"\u27A1" and current_page < len(embed_pages) - 1:
                        current_page += 1
                    elif reaction.emoji == u"\u23E9":
                        current_page = len(embed_pages) - 1

                    # remove the added reactions
                    for button in buttons:
                        await list_message.remove_reaction(button, ctx.author)

                    # edit the message
                    if current_page != previous_page:
                        await list_message.edit(embed=embed_pages[current_page])

    async def remind(self, reminder):
        await asyncio.sleep((reminder._get_datetime() - datetime.now()).total_seconds())
        reminder_channel = self.get_channel(reminder.reminder_channel)
        await reminder_channel.send(f"{' '.join(reminder.get_tags())} {reminder.get_text()}")
        self.db.delete(reminder)


if __name__ == '__main__':

    DB_FILE = "reminders.json"
    COMMAND_PREFIX = "!"

    bot = EdithBot(COMMAND_PREFIX)


    @tasks.loop(minutes=1)
    async def reminder_executor():
        print("--- new loop ---")

        next_reminder = bot.queue.get_next_reminder()
        while next_reminder is not None and next_reminder.is_in_ten_minutes():
            bot.queue.remove_reminder(next_reminder)
            await bot.remind(next_reminder)
            next_reminder = bot.queue.get_next_reminder()


    @bot.event
    async def on_ready():
        print(f"Logged in as {bot.user}")

        # TODO fill the q from the database

        print("Starting reminder executor!")
        reminder_executor.start()


    bot.run(bot.token)
