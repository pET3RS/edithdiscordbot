import unittest
from edith import *
from exceptions import *
from datetime import date
import random
from calendar import monthrange


class TestReminder(unittest.TestCase):

    def test_reminder_id(self):
        new_reminder = Reminder(author_example, f"D{date_example}T{time_example}", "Hello there!")
        self.assertTrue(new_reminder.get_id() is None)

    def test_reminder_author(self):

        # Creating a reminder with a valid author
        for valid_author, valid_date, valid_time in zip(valid_authors, valid_dates, valid_times):
            new_reminder = Reminder(valid_author, f"D{valid_date}T{valid_time}", "OK")
            self.assertEqual(new_reminder.get_author(), valid_author)

        # Creating a reminder with an invalid author
        for invalid_author, valid_date, valid_time in zip(invalid_authors, valid_dates, valid_times):
            with self.assertRaises(ReminderTagErr):
                Reminder(invalid_author, f"D{valid_date}T{valid_time}", "Wrong author")

    def test_reminder_date_and_time(self):

        # Creating a reminder with valid and full date_time input
        for valid_author, valid_date, valid_time in zip(valid_authors, valid_dates, valid_times):
            new_reminder = Reminder(valid_author, f"D{valid_date}T{valid_time}", "OK - full date & time input")
            self.assertEqual(new_reminder.get_date_time(), f"D{valid_date}T{valid_time}")

        # Creating a reminder with valid partial (time only) date_time input
        for valid_author, valid_time in zip(valid_authors, valid_times):
            new_reminder = Reminder(valid_author, f"T{valid_time}", "OK - only time input")
            self.assertEqual(new_reminder.get_date_time(), "D"+date.today().strftime("%d.%m.%Y")+f"T{valid_time}")

        # Creating a reminder with invalid date_time format
        for valid_author, invalid_date_time in zip(valid_authors, invalid_date_times):
            with self.assertRaises(ReminderDateTimeErr):
                Reminder(f"{author_example}", invalid_date_time, "Wrong - invalid date_time format")

        # Creating a reminder with invalid date
        for valid_author, invalid_date, valid_time in zip(valid_authors, invalid_dates, valid_times):
            with self.assertRaises(ReminderDateTimeErr):
                Reminder(valid_author, f"D{invalid_date}T{valid_time}", "Wrong - invalid date")

        # Creating a reminder with invalid time
        for valid_author, valid_date, invalid_time in zip(valid_authors, valid_dates, invalid_times):
            with self.assertRaises(ReminderDateTimeErr):
                Reminder(valid_author, f"D{valid_date}T{invalid_time}", "Wrong - invalid time")

    def test_reminder_adding_tags(self):

        new_reminder = Reminder(author_example, f"D{date_example}T{time_example}", "OK reminder")

        # Adding an ok tag to a reminder
        for valid_tag in valid_tags:
            new_reminder.add_tag(valid_tag)
            self.assertEqual(len(new_reminder.get_tags()), 1)
            self.assertEqual(new_reminder.get_tags().pop(), valid_tag)

        # Adding a wrong tag to a reminder
        for invalid_tag in invalid_tags:
            with self.assertRaises(ReminderTagErr):
                new_reminder.add_tag(invalid_tag)
            self.assertEqual(len(new_reminder.get_tags()), 0)

    def test_reminder_removing_tags(self):

        # setup
        new_reminder = Reminder(author_example, f"D{date_example}T{time_example}", "OK reminder")
        new_reminder._tags = set(valid_tags)

        # Removing an ok tag from a reminder
        current_tags = set(valid_tags)
        for valid_tag in valid_tags:
            current_tags.remove(valid_tag)
            new_reminder.remove_tag(valid_tag)
            self.assertEqual(new_reminder.get_tags(), current_tags)

        self.assertEqual(len(new_reminder.get_tags()), 0)

        with self.assertRaises(ReminderTagErr):
            new_reminder.remove_tag(invalid_tags[0])

        self.assertEqual(len(new_reminder.get_tags()), 0)

    def test_reminder_changing_time(self):

        # Setup
        new_reminder = Reminder(f"{author_example}", f"D{date_example}T{time_example}", "Hello there!")

        # Changing date&time with complete valid date and time input
        for valid_author, valid_date, valid_time in zip(valid_authors, valid_dates, valid_times):
            new_reminder.change_date_time(f"D{valid_date}T{valid_time}")
            self.assertEqual(new_reminder.get_date_time(), f"D{valid_date}T{valid_time}")

        # Changing date&time with partial (only time) valid date and time input
        current_date_time = new_reminder.get_date_time()
        for valid_author, valid_time in zip(valid_authors, valid_times):
            new_reminder.change_date_time(f"T{valid_time}")
            self.assertEqual(new_reminder.get_date_time(), current_date_time.split("T")[0] + f"T{valid_time}")

        # Changing date&time with partial (only date) valid date and time input
        current_date_time = new_reminder.get_date_time()
        for valid_author, valid_date in zip(valid_authors, valid_dates):
            new_reminder.change_date_time(f"D{valid_date}")
            self.assertEqual(new_reminder.get_date_time(), "D"+valid_date+"T"+current_date_time.split("T")[1])

        # Changing date&time with invalid date_time format
        current_date_time = new_reminder.get_date_time()
        for valid_author, invalid_date_time in zip(valid_authors, invalid_date_times):
            with self.assertRaises(ReminderDateTimeErr):
                new_reminder.change_date_time(invalid_date_time)
            self.assertEqual(new_reminder.get_date_time(), current_date_time)

        # Changing date&time with invalid date
        current_date_time = new_reminder.get_date_time()
        for valid_author, invalid_date, valid_time in zip(valid_authors, invalid_dates, valid_times):
            with self.assertRaises(ReminderDateTimeErr):
                new_reminder.change_date_time(f"D{invalid_date}T{valid_time}")
            self.assertEqual(new_reminder.get_date_time(), current_date_time)

        # Creating date&time with invalid time
        for valid_author, valid_date, invalid_time in zip(valid_authors, valid_dates, invalid_times):
            with self.assertRaises(ReminderDateTimeErr):
                new_reminder.change_date_time(f"D{valid_date}T{invalid_time}")
            self.assertEqual(new_reminder.get_date_time(), current_date_time)

    def test_reminder_equality(self):
        new_reminder1 = Reminder(f"{author_example}", f"D{date_example}T{time_example}", "Hello there!")
        new_reminder2 = Reminder(f"{author_example}", f"D{date_example}T{time_example}", "Hello there!")
        self.assertTrue(new_reminder1 == new_reminder2)


class TestReminderQueue(unittest.TestCase):

    def setUp(self):
        self.reminder_queue = ReminderQueue()
        self.reminders = []

        for i in range(5):
            new_reminder = Reminder(valid_authors[i], f"D{valid_dates[i]}T{valid_times[i]}", f"Text{i + 1}")
            new_reminder.add_tag(valid_authors[i])
            self.reminders.append(new_reminder)

        self.reminder_queue._queue = sorted(self.reminders, reverse=True)

    def test_reminder_queue_add_reminder(self):
        new_reminder = Reminder(author_example, f"D{date_example}T{time_example}", "New reminder for the Q")
        self.reminder_queue.add_reminder(new_reminder)

        current_queue = self.reminders
        current_queue.append(new_reminder)
        current_queue = sorted(current_queue, reverse=True)

        for q_reminder, reminder in zip(self.reminder_queue.get_queue(), current_queue):
            self.assertTrue(q_reminder == reminder)
            self.assertEqual(q_reminder.get_author(), reminder.get_author())
            self.assertEqual(q_reminder.get_tags(), reminder.get_tags())

        with self.assertRaises(ReminderExistsErr):
            self.reminder_queue.add_reminder(new_reminder)

    def test_reminder_queue_remove_reminder(self):

        current_queue = self.reminder_queue.get_queue().copy()

        for reminder in current_queue:
            self.reminder_queue.remove_reminder(reminder)
            self.assertTrue(reminder not in self.reminder_queue.get_queue())

            with self.assertRaises(ReminderDoesntExistErr):
                self.reminder_queue.remove_reminder(reminder)

    def test_reminder_queue_get_next_reminder(self):

        next_reminder = self.reminder_queue.get_next_reminder()
        self.reminders.remove(next_reminder)

        for reminder in self.reminders:
            self.assertTrue(reminder >= next_reminder)


class TestReminderDB(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.db = ReminderDB("reminder-tests.json")

    @classmethod
    def tearDownClass(cls):
        os.remove("reminder-tests.json")

    def setUp(self):
        self.reminders = []

        #for i, (valid_author, valid_date, valid_time) in enumerate(zip(valid_authors, valid_dates, valid_times)):
        for i in range(5):
            new_reminder = Reminder(valid_authors[i], f"D{valid_dates[i]}T{valid_times[i]}", f"Text{i+1}")
            new_reminder.add_tag(valid_authors[i])
            new_reminder_id = self.db.reminders_table.insert(new_reminder.get_db_dict())
            new_reminder.change_id(new_reminder_id)
            self.reminders.append(new_reminder)

        self.test_reminder = Reminder(author_example, f"D{date_example}T{time_example}", f"Testing reminder")
        self.test_reminder.add_tag(author_example)

    def tearDown(self):
        self.db.tiny_db.truncate()

    def test_push(self):

        self.db.push(self.test_reminder)
        self.assertTrue(self.test_reminder.get_id() is not None)
        self.assertEqual(self.test_reminder.get_db_dict(),
                         self.db.reminders_table.get(doc_id=self.test_reminder.get_id()))

        with self.assertRaises(ReminderExistsErr):
            self.db.push(self.test_reminder)

    def test_update(self):

        updated_reminder = self.reminders[0]
        updated_reminder.change_date_time(f"D{date_example}T{time_example}")
        updated_reminder.add_tag(author_example)
        updated_reminder.change_text("Updated this reminder")

        self.db.update(updated_reminder)
        updated_reminder_dict_from_db = self.db.reminders_table.get(doc_id=updated_reminder.get_id())
        self.assertNotEqual(updated_reminder_dict_from_db, None)

        self.assertEqual(updated_reminder_dict_from_db.get("author"), updated_reminder.get_author())
        self.assertEqual(updated_reminder_dict_from_db.get("date_time"), updated_reminder.get_date_time())
        self.assertEqual(updated_reminder_dict_from_db.get("tags"), list(updated_reminder.get_tags()))
        self.assertEqual(updated_reminder_dict_from_db.get("text"), updated_reminder.get_text())

    def test_delete(self):
        for reminder in self.reminders:
            deleted_reminder_id = reminder.get_id()
            self.db.delete(reminder)
            self.assertTrue(reminder.get_id() is None)
            self.assertTrue(self.db.reminders_table.get(doc_id=deleted_reminder_id) is None)

    def test_id_query(self):
        for reminder in self.reminders:
            searched_reminder = self.db.id_query(reminder.get_id())
            self.assertTrue(reminder == searched_reminder)

    def test_date_time_query(self):
        for reminder in self.reminders:
            searched_reminder_list = self.db.date_time_query(reminder.get_date_time())
            self.assertTrue(searched_reminder_list is not None)
            self.assertTrue(reminder in searched_reminder_list)

    def test_date_time_text_query(self):
        for reminder in self.reminders:
            searched_reminder_list = self.db.date_time_text_query(reminder.get_date_time(), reminder.get_text())
            self.assertTrue(searched_reminder_list is not None)
            self.assertTrue(reminder in searched_reminder_list)


if __name__ == '__main__':

    def generate_date():
        year = random.randint(int(datetime.today().year) + 1, 5000)
        month = random.randint(1, 12)
        day = random.randint(1, monthrange(year, month)[1])
        return {"day": day,
                "month": month,
                "year": year}


    def generate_time():
        hour = random.randint(int(datetime.now().hour), 23)
        minute = random.randint(int(datetime.now().minute+1), 59)
        return {"hour": hour,
                "minute": minute}


    valid_dates = []
    for _ in range(5):
        new_date = generate_date()
        valid_dates.append(f"{new_date.get('day')}.{new_date.get('month')}.{new_date.get('year')}")
    invalid_dates = ["1.8.2000", "a.3.0b00", "93.12.2039", "01.13.2024", "00.00.1111"]

    valid_times = []
    for idx in range(5):
        new_time = generate_time()
        valid_times.append(f"{new_time.get('hour')}:{new_time.get('minute'):02}")
    invalid_times = ["a0:00", "12.10", "24:00", "999:001", "1:60"]

    invalid_date_times = ["D21.08.2021T", "21.08.202108:00", "T21.08.2021D08:00",
                          "D09.1.2021T999:0111", "D0000.1000.2021T12:00"]

    valid_authors = valid_tags = ["theptr#1234", "abcd#8502", "Nick#0294", "noobmaster69#9087", "<@!188732454691930112>"]
    invalid_authors = invalid_tags = ["ptr", "#1234", "ptr#123", "ptr#12345", "<@!&123>"]

    new_time = generate_time()
    new_date = generate_date()
    time_example = f"{new_time.get('hour')}:{new_time.get('minute'):02}"
    date_example = f"{new_date.get('day')}.{new_date.get('month')}.{new_date.get('year')}"
    author_example = "ptr#1234"

    unittest.main()
