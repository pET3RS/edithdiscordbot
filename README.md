# EDITH DISCORD BOT

Yet another discord bot; a free time project intended for personal use.

## Features

* reminders
    - create a reminder with specific date and time, tag people or roles
    - get information about reminder
    - edit the created reminder with multiple options (editing the whole reminder or only its parts)
    - list all upcomming reminders in an embeded message with pages
    - the reminders are stored in a simple database

### Future features
- [ ] Add events to the reminders feature which could create multiple reminders based on input
- [ ] Custom polls
- [ ] LMGTFY command
- [ ] play youtube video through screen share
